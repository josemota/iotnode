#!/usr/bin/python3
# -*- coding: utf-8; mode: python -*-

import sys
import Ice
import json
import paho.mqtt.client as mqtt
import logging
import time
import re
from xml.dom import minidom

logging.basicConfig(stream=sys.stdout)
logger = logging.getLogger("SMART_CITY_PLATFORM")
logger.setLevel(logging.INFO)


class SMART_CITY_PLATFORM(Ice.Application):
    def __init__(self, mqtt_client, config):
        self.mqtt_client = mqtt_client
        self.mqtt_client.on_connect = self.on_connect
        self.mqtt_client.on_message = self.on_message
        self.config = config

    def run(self):
        try:
            self.mqtt_client.connect(self.config['broker_addr'], int(self.config['broker_port']), 60)
            self.mqtt_client.loop_forever()
        except KeyboardInterrupt:
            self.mqtt_client.disconnect()

    def on_connect(self, client, userdata, flags, rc):
        logger.info("Connected with result code " + str(rc))
        mqtt_topics = self.config['mqtt_sub_topics']
        for topic in mqtt_topics:
            client.subscribe(topic.strip())
            logger.info("Subscribed to MQTT: " + topic)
        logger.info("Ready, waiting events...")

    def on_message(self, client, userdata, msg):

        # If a smoke alarm is detected
        regex = re.compile ('event/uclm/escuela_superior_de_informatica/fermin_caballero/3600/*')
        if re.match(regex, msg.topic):
            rcvdMsg = minidom.parseString(msg.payload.decode())
            messageValue = rcvdMsg.getElementsByTagName("Resources")[0].getElementsByTagName("Item")[0].firstChild.data
            #HardCoded turn on emergency lights message
            normMsg = """<?xml version="1.0" encoding="UTF-8"?>
            <Object ObjectType="Command">
                <GlobalID>3510000000000010</GlobalID>
                <ObjectID>3306</ObjectID>
                        
                <Resources>
                    <Item ID="5850">{}</Item>
                </Resources>
                            
                <Metadata>
                    <Quality>255</Quality>
                    <Longitude>-3.9319265</Longitude>
                    <Altitude>650.0</Altitude>
                    <Timestamp>2019-10-13 17:53:22.627560</Timestamp>
                    <Place>ciudad_real.13001.plaza_mayor.1.ayuntamiento_de_ciudad_real.2.cpd</Place>
                    <Expiration>600</Expiration>
                    <Latitude>38.978285</Latitude>
                </Metadata>
                            
                <MessageType>command</MessageType>
            </Object>
            """
            self.mqtt_client.publish("command/uclm/escuela_superior_de_informatica/fermin_caballero/5850/3510000000000010", normMsg.format(messageValue))

if __name__ == "__main__":
    if len(sys.argv) != 2:
        logger.info("Usage: {} <mqtt-adapter.json>".format(sys.argv[0]))
        exit(1)

    json_config = open(sys.argv[1]).read()
    config = json.loads(json_config)
    mqtt_client = mqtt.Client(config['client'])

    broker = SMART_CITY_PLATFORM(mqtt_client, config)
    broker.run()