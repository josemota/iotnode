module IoTNode{
    sequence<byte> ByteSeq;

    interface SemanticAbstractionService {
        void abstractMessage(string ID, ByteSeq message);
    };

    interface SemanticConcretionService {
        void concreteMessage(string message);
    };

    interface RegistryService {
        bool isRegistered (string ID);
        string getEntry (string ID);
        string getTypeDescription (string IPSOType);
    };

    interface SCPCommunicationMQTTService {
        void sendEvent(string message);
        void sendTelemetry(string message);
    };

    interface ZDOutputService {
        void setLights(bool state);
    };
};