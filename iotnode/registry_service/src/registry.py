#!/usr/bin/python3
# -*- coding: utf-8; mode: python -*-

import sys
import os.path
import Ice
import json
import logging
from xml.dom import minidom

logging.basicConfig(stream=sys.stdout)
logger = logging.getLogger("REGISTRY")
logger.setLevel(logging.INFO)

Ice.loadSlice('IoTNode.ice')
import IoTNode

class RegistryServiceI(IoTNode.RegistryService):
    def __init__(self):
        with open('../devices/registry.json') as json_file:
            self.registered_devices = json.load(json_file)

    def isRegistered(self, ID, current=None):
        if ID in self.registered_devices:
            return True
        else:
            return False

    def getEntry (self, ID, current=None):
        filePath = ""
        if ID in self.registered_devices:
            filePath = "../devices/"+self.registered_devices[ID]
            devEntry = minidom.parse(filePath)
            return devEntry.toprettyxml(encoding='UTF-8').decode()
        else:
            return None

    def getTypeDescription (self, IPSOType, current=None):
        filePath = "../device_types/"+IPSOType+".xml"

        if os.path.exists(filePath):
            typeDesc = minidom.parse(filePath)
            return typeDesc.toprettyxml(encoding='UTF-8').decode()
        else:
            return None

class REGISTRY(Ice.Application):
    def run(self, argv):
        logger.info("registry service")
        ice = self.communicator()
        servant = RegistryServiceI()

        adapter = ice.createObjectAdapter("Adapter")
        proxy = adapter.add(servant, ice.stringToIdentity("registry"))

        adapter.activate()
        self.shutdownOnInterrupt()
        ice.waitForShutdown()

        return 0
    
registry = REGISTRY()
sys.exit(registry.main(sys.argv))