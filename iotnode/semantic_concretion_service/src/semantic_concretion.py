#!/usr/bin/python3
# -*- coding: utf-8; mode: python -*-

import sys
import Ice
import json
import logging
from xml.dom import minidom
import datetime

logging.basicConfig(stream=sys.stdout)
logger = logging.getLogger("SEM_CONC")
logger.setLevel(logging.INFO)

Ice.loadSlice('IoTNode.ice')
import IoTNode

class SemanticConcretionServiceI(IoTNode.SemanticConcretionService):
    def __init__(self, ice, registry_prx):
        self.ice = ice
        self.registry = IoTNode.RegistryServicePrx.checkedCast(registry_prx)
        # self.zdOutput = IoTNode.ZDOutputServicePrx.checkedCast(zdOutput_prx)

    def concreteMessage(self, message, current = None):
        logger.info("message received for concretion")
        normMsg = minidom.parseString(message)
        devEntry = self.getDevEntry(normMsg.getElementsByTagName("GlobalID")[0].firstChild.data)
        
        #get every item in the command and its casted value
        itemsAndValues = {}
        items = normMsg.getElementsByTagName("Resources")[0].getElementsByTagName("Item")
        for item in items:
            value = self.getValue(devEntry, item)
            itemsAndValues[item.attributes['ID'].value] = value
        logger.info(itemsAndValues)

        #now get the corresponding plugin to make the corresponding call
        xmlPlugin = devEntry.getElementsByTagName("Command")[0].getElementsByTagName("Plugin")[0]
        if xmlPlugin.getElementsByTagName("PType")[0].firstChild.data.lower() == "ice":

            plugin = getattr(IoTNode, xmlPlugin.attributes['ID'].value)
            
            strPrx = xmlPlugin.getElementsByTagName("Proxy")[0].firstChild.data
            logger.info(strPrx)
            prx = self.ice.stringToProxy(strPrx)
            proxy = plugin.checkedCast(prx)
            call = getattr(proxy, xmlPlugin.getElementsByTagName("Call")[0].attributes['ID'].value)

            #create ordered list with parameters
            orderedList = []
            params = xmlPlugin.getElementsByTagName("Call")[0].getElementsByTagName("Param")
            for param in params:
                orderedList.append(itemsAndValues[param.firstChild.data])

            logger.info(orderedList)
            call(*orderedList)

    def to_bool(self, value):
        if value.lower() == 'true':
            return True
        elif value.lower() == 'false':
            return False

    def getValue(self, devEntry, item):
        itemID = item.attributes['ID'].value
        items = devEntry.getElementsByTagName('Command')[0].getElementsByTagName('Item')
        
        mapCast = {"boolean": self.to_bool, "int": int, "float":float, "string":str}
        for i in items:
            if i.attributes['ID'].value == itemID:
                dType = i.getElementsByTagName('DType')[0].firstChild.data.lower()
                value = mapCast[dType](item.firstChild.data.lower())
        return value

    def getDevEntry(self, globalID):
        logger.info("getting device entry")
        strEntry = self.registry.getEntry(globalID)
        devEntry = minidom.parseString(strEntry)
        return devEntry

class SEM_CONC(Ice.Application):
    def run(self, argv):
        logger.info("semantic concretion service---------------------")
        ice = self.communicator()

        registryPrx = ice.propertyToProxy("Registry.Proxy")

        servant = SemanticConcretionServiceI(ice, registryPrx)
        adapter = ice.createObjectAdapter("Adapter")
        proxy = adapter.add(servant, ice.stringToIdentity("semConc"))

        adapter.activate()
        self.shutdownOnInterrupt()
        ice.waitForShutdown()

        return 0

sem_conc = SEM_CONC()
sys.exit(sem_conc.main(sys.argv))
