#!/usr/bin/python3
# -*- coding: utf-8; mode: python -*-

import sys
import Ice
import json
import paho.mqtt.client as mqtt
import logging

logging.basicConfig(stream=sys.stdout)
logger = logging.getLogger("ACQ_MQTT")
logger.setLevel(logging.INFO)

Ice.loadSlice('IoTNode.ice')
import IoTNode

class ACQ_MQTT(Ice.Application):
    def __init__(self, mqtt_client, config):
        self.mqtt_client = mqtt_client
        self.mqtt_client.on_connect = self.on_connect
        self.mqtt_client.on_message = self.on_message
        self.config = config

    def run(self):
        try:
            self.communicator = Ice.initialize(sys.argv)
            sem_abs_prx = self.communicator.propertyToProxy("SemanticAbstractionService.Proxy")
            self.sem_abs = IoTNode.SemanticAbstractionServicePrx.checkedCast(sem_abs_prx)

            logger.info("MQTT server: " + self.config['broker_addr'])
            self.mqtt_client.connect(self.config['broker_addr'])
            self.mqtt_client.loop_forever()
        except KeyboardInterrupt:
            self.mqtt_client.disconnect()

    def on_connect(self, client, userdata, flags, rc):
        logger.info("Connected with result code " + str(rc))
        mqtt_topics = self.config['mqtt_topics']
        for topic in mqtt_topics:
            client.subscribe(topic.strip())
            logger.info("Subscribed to MQTT: " + topic)
        logger.info("Ready, waiting events...")

    def on_message(self, client, userdata, msg):
        self._print_message_info(msg)
        topic = msg.topic
        payload = msg.payload
        self.sem_abs.abstractMessage(topic, payload)

    def _print_message_info(self, msg):
        logger.info("\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        logger.info("New MQTT message received:")
        logger.info("MQTT topic: " + msg.topic)
        logger.info("Message: " + msg.payload.decode())
        logger.info("\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        logger.info("Usage: {} --Ice.Config=<ice.config> <mqtt-adapter.json>".format(sys.argv[0]))
        exit(1)

    json_config = open(sys.argv[2]).read()
    config = json.loads(json_config)
    mqtt_client = mqtt.Client(config['client'])

    aquisitor = ACQ_MQTT(mqtt_client, config)
    aquisitor.run()
