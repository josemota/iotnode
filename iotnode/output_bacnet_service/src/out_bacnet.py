import os

from bacpypes.debugging import bacpypes_debugging, ModuleLogger, xtob

from bacpypes.core import run, stop, deferred
from bacpypes.task import TaskManager
from bacpypes.comm import PDU, Client, Server, bind, ApplicationServiceElement

# settings
SERVER_HOST = os.getenv('SERVER_HOST', '127.0.0.1')
SERVER_PORT = int(os.getenv('SERVER_PORT', 9000))
CONNECT_TIMEOUT = int(os.getenv('CONNECT_TIMEOUT', 0)) or None
IDLE_TIMEOUT = int(os.getenv('IDLE_TIMEOUT', 0)) or None


class MiddleMan(Client, Server):
    """
    An instance of this class sits between the TCPClientDirector and the
    console client.  Downstream packets from a console have no concept of a
    destination, so this is added to the PDUs before being sent to the
    director.  The source information in upstream packets is ignored by the
    console client.
    """
    def indication(self, pdu):
        if _debug: MiddleMan._debug("indication %r", pdu)
        global server_address

        # no data means EOF, stop
        if not pdu.pduData:
            # ask the director (downstream peer) to close the connection
            self.clientPeer.disconnect(server_address)
            return

        # pass it along
        self.request(PDU(pdu.pduData, destination=server_address))

    def confirmation(self, pdu):
        if _debug: MiddleMan._debug("confirmation %r", pdu)

        # check for errors
        if isinstance(pdu, Exception):
            if _debug: MiddleMan._debug("    - exception: %s", pdu)
            return

        # pass it along
        self.response(pdu)

class MyClient(Client):
    def confirmation(self, pdu):
        print('thanks for the ', pdu)

c = MyClient()
bind(c, s)
c.request('hello')