#!/usr/bin/python3
# -*- coding: utf-8; mode: python -*-

import sys
import Ice
import json
import paho.mqtt.client as mqtt
import logging
from xml.dom import minidom

logging.basicConfig(stream=sys.stdout)
logger = logging.getLogger("SCP_COMMUNICATION_MQTT_SERVICE")
logger.setLevel(logging.INFO)

Ice.loadSlice('IoTNode.ice')
import IoTNode

class SCPCommunicationMQTTServiceI(IoTNode.SCPCommunicationMQTTService):
    def __init__(self, mqtt_client, config, sem_conc_prx):
        self.mqtt_client = mqtt_client
        self.mqtt_client.on_connect = self.on_connect
        self.mqtt_client.on_message = self.on_message
        self.config = config
        self.sem_conc = IoTNode.SemanticConcretionServicePrx.checkedCast(sem_conc_prx)

    def run(self):
        try:
            logger.info("MQTT server: " + self.config['broker_addr'])
            self.mqtt_client.connect(self.config['broker_addr'], int(self.config['broker_port']), 60)
            self.mqtt_client.loop_forever()

        except KeyboardInterrupt:
            self.mqtt_client.disconnect()

    def on_connect(self, client, userdata, flags, rc):
        logger.info("Connected with result code " + str(rc))
        for topic in self.config['mqtt_sub_topics']:
            client.subscribe(topic.strip())
            logger.info("Subscribed to MQTT: " + topic)
        logger.info("Ready, waiting events...")

    def on_message(self, client, userdata, msg):
        logger.info("sending message for concretion")
        self.sem_conc.concreteMessage(msg.payload.decode())

    def sendEvent(self, message, current=None):
        logger.info(message)
        xml = minidom.parseString(message)
        objectID = xml.getElementsByTagName("ObjectID")[0].firstChild.data
        globalID = xml.getElementsByTagName("GlobalID")[0].firstChild.data
        topic = self.config['mqtt_event_pub_topic'][0]+objectID+"/"+globalID
        logger.info(message)
        logger.info(topic)
        self.mqtt_client.publish(topic, message)
        logger.info("event published")

    def sendTelemetry(self, message, current=None):
        xml = minidom.parseString(message)
        objectID = xml.getElementsByTagName("ObjectID")[0].firstChild.data
        globalID = xml.getElementsByTagName("GlobalID")[0].firstChild.data
        topic = self.config['mqtt_telemetry_pub_topic'][0]+objectID+"/"+globalID
        self.mqtt_client.publish(topic, message)
        logger.info("telemetry published")


class SCPCommunicationMQTTService(Ice.Application):
    def run(self, argv):
        logger.info("scp communication MQTT service")
        ice = self.communicator()
        sem_conc_prx = ice.propertyToProxy("SemanticConcretionService.Proxy")

        # Load the mqtt config
        json_config = open(sys.argv[1]).read()
        config = json.loads(json_config)
        mqtt_client = mqtt.Client(config['client'])

        servant = SCPCommunicationMQTTServiceI(mqtt_client, config, sem_conc_prx)
        adapter = ice.createObjectAdapter("Adapter")
        adapter.add(servant, ice.stringToIdentity("SCPCommMQTT"))
        adapter.activate()
        servant.run()

        self.shutdownOnInterrupt()
        ice.waitForShutdown()

        return 0


if __name__ == "__main__":
    if len(sys.argv) != 3:
        logger.info("Usage: {} --Ice.Config=<ice.config> <mqtt-adapter.json>".format(sys.argv[0]))
        exit(1)
    
    scp = SCPCommunicationMQTTService()
    sys.exit(scp.main(sys.argv))
