from pymongo import MongoClient
import random

# Connect to database
client = MongoClient('localhost', 27017)

# Create database
db = client["events"]

# Check if database exists
dblist = client.list_database_names()
if "events" in dblist:
  print("The database exists.")

# Create a collection
collection = db["events"]

# Check if collection exists
collections = db.list_collection_names()
if "events" in collections:
  print("The collection exists.")

# Insert an event
# event = { "sensor": "Sonoff", "time": random.randint(0, 10000) }
# x = collection.insert_one(event)
# print(x.inserted_id)

# Find events
for x in collection.find({}, { "sensor": "Sonoff"}):
  print(x)