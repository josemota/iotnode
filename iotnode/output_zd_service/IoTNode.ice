module IoTNode{

    enum MetadataField {
        Timestamp,
        Quality,
        Expiration,
        Latitude,
        Longitude,
        Altitude,
        Place
    };

    dictionary <MetadataField, string> Metadata;
    sequence<byte> ByteSeq;

    interface SemanticAbstractionService {
        void abstractMessage(string ID, ByteSeq message);
    };

    interface RegistryService {
        bool isRegistered (string ID);
        string getEntry (string ID);
        string getTypeDescription (string IPSOType);
    };

    interface SCPCommunicationMQTTService {
        void sendMessage(string message);
    };

    interface ZDOutputService {
        void setLights(bool state);
    };
};