import sys
import logging
import Ice
import zd_controller

logging.basicConfig(stream=sys.stdout)
logger = logging.getLogger('ZDOutputService')
logger.setLevel(logging.DEBUG)

class Service(Ice.Application):
    def run(self, argv):
        logger.info("ZD output service")
        ice = self.communicator()

        servant = zd_controller.ZDOutputServiceI()

        adapter = ice.createObjectAdapter("Adapter")
        proxy = adapter.add(servant, ice.stringToIdentity("zdOutput"))

        adapter.activate()
        self.shutdownOnInterrupt()
        logger.debug("Server activated")
        ice.waitForShutdown()
        return 0

if __name__ == '__main__':
    service = Service()
    sys.exit(service.main(sys.argv))
