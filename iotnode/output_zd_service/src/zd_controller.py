import Ice
Ice.loadSlice('IoTNode.ice')
import IoTNode
import zemper_transceiver

import logging

class ZDOutputServiceI(IoTNode.ZDOutputService):
    def __init__(self):
        self.trasceiver = zemper_transceiver.ZemperTransceiver('/dev/ttyUSB0', baudrate=57000)
        self.logger = logging.getLogger("ZDService")

    def setLights(self, state, context=None):
        self.logger.debug("Action received")
        if state:
            self.trasceiver.send_action(255, 'light_on', zone=1)
        else:
            self.trasceiver.send_action(255, 'light_off', zone=1)