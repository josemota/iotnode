import logging
import time
import serial
from libzpprotocol import packets

class PySerialController:
    def __init__(self, baudrate=57600, address='/dev/ttyUSB0',
                 timeout=10):
        self.address = address
        self.baudrate = baudrate
        self.timeout = timeout
        self.controller = serial.Serial(address, timeout=0,
                                        baudrate=baudrate)

    def write(self, packet_to_be_sent):
        return self.controller.write(packet_to_be_sent)

    def read(self, number_of_bytes):
        data = self.controller.read(number_of_bytes)
        return (data, len(data))

    def __repr__(self):
        return "PySerialController: baudrate = {}, address = {}, timeout = {}".format(
            self.baudrate, self.address, self.timeout)


class ZemperTransceiver():
    def __init__(self, device='/dev/ttyACM0', baudrate=57000):
        self.device = device
        self.baudrate = baudrate
        self.setup_transceiver()
        self.logger = logging.getLogger('ZemperTranceiver')
        self.logger.setLevel(logging.DEBUG)

    def setup_transceiver(self):
        self.transceiver = PySerialController(address=self.device, baudrate=self.baudrate)

    
    def read_packet(self, zone, timeout=1):
        """ This method implements a blocking state machine to decode a zd packet.

        All zp packets has the following format:
        * init byte: 0xfa
        * Sync bytes: 0x55, 0x55
        * Payload: it depends of the packet
        * End byte: 0xfe

        If it is not possible to decode all packet in ``timeout`` seconds it will
        return None.

        Note:
            It is possible that if it is not posssible to decode entire packet
            in ``timeout`` seconds the next reads could be wrong too.

        Args:
            zone (int): Zone where packet will be read.
            timeout (int): Timeout in which entire packet should be fetched.

        Returns:
            bytes: None of bytes.

        """

        transceiver = self.transceiver

        self.logger.debug('Trying to read packet with timeout of {} seconds'.format(timeout))
        buffer_of_bytes = []

        last_time = time.time()
        last_byte_received = False

        state = 'init'

        while (time.time() - last_time) < timeout:

            with self.read_stop_lock:
                if self.stop_read_flag:
                    self.stop_read_flag = False
                    break

            # Read without block
            b, length = transceiver.read(1)

            # Until I have not read first byte..
            if state is 'init':
                if int.from_bytes(b, 'big') == 0xFA:
                    buffer_of_bytes.append(0xFA)
                    state = 'first_sync'
                    continue

                elif int.from_bytes(b, 'big') == 0x55:
                    buffer_of_bytes.append(0xFA)
                    buffer_of_bytes.append(0x55)
                    state = 'second_sync'
                    continue
                else:
                    if b != b'':
                        self.logger.debug('Garbage in bus {}'.format(b.hex()))

            if state is 'first_sync':
                if int.from_bytes(b, 'big') == 0x55:
                    buffer_of_bytes.append(0x55)
                    state = 'second_sync'
                    continue

            if state is 'second_sync':
                if length > 0:
                    buffer_of_bytes.append(0x55)
                    state = 'read'
                    if int.from_bytes(b, 'big') != 0x55:
                        buffer_of_bytes.append(int.from_bytes(b, 'big'))

                    continue

            if length > 0 and state == 'read':
                buffer_of_bytes.append(int.from_bytes(b, 'big'))
                # Packets are terminated by 0xFE
                if int.from_bytes(b, 'big') == 0xFE:
                    self.logger.debug('Byte 0xFE read')
                    last_byte_received = True
                    return bytearray(buffer_of_bytes)

        # If timeout has been reached and I dont have last_byte
        # This means that there is an error in bus (or it has been
        # imposible to read command in that time
        if last_byte_received is False:
            self.logger.debug('Impossible to read complete packet. Current {}'.format(bytearray(buffer_of_bytes).hex()))
            return None

    def write_packet(self, zone, packet_stream):
        self.logger.debug('Writting: {}'.format(packet_stream.hex()))
        self.transceiver.controller.flushInput()
        self.transceiver.controller.reset_output_buffer()
        bytes_written = None
        dt = (10 * len(packet_stream) + 4)/57600.0
        time.sleep(0.100)
        bytes_written = None
        try:
            before = time.time()
            bytes_written = self.transceiver.write(packet_stream)
            while time.time() - before < dt:
                pass

            self.logger.debug('dt: {}'.format(time.time() - before))
            return len(packet_stream)

        except Exception as ex:
            self.logger.debug(ex)

        return bytes_written

    def _send_request(self, packet, zone):
        stream = packet.serialize_request()
        self.logger.debug("==> Request: {}".format(stream.hex()))

        expected_size = packet.get_size('request')
        bytes_written = self.write_packet(zone, stream)
        if bytes_written != expected_size:
            self.logger.debug('Output error bytes written != expected')
            return False

        return True

    def _request_noreply(self, request_packet, zone, timeout, attempts, delay_between_tries):

        for attempt in range(0, attempts):
            if attempt is not 0:
                time.sleep(delay_between_tries)

            if not self._send_request(request_packet, zone):
                continue

    def send_action(self, light_id, action, zone):
        attempts = 5
        request_packet = packets.PacketAction(light_id=light_id, action=action)
        self.logger.debug("Sending action: {}".format(request_packet))

        self._request_noreply(request_packet, zone, 1, attempts, 0.5)