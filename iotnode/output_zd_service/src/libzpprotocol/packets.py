import struct
import logging
from . import constants as c
from . import exceptions as ex

logger = logging.getLogger('zemper.libzpprotocol.packets')


def statuscode_2_message(code):
    return c.hub_status_messages[code]


class PacketAction:
    allowed_actions = ['light_on', 'light_off',
                       'reset', 'test_light',
                       'test_light', 'test_autonomy',
                       'test_even', 'test_odd',
                       'zone_in_test_on', 'zone_in_test_off',
                       'reset_counters', 'error']

    from_action_str_2_code = {
        'light_on': c.TX_LON,
        'light_off': c.TX_LOFF,
        'reset': c.TX_RST,
        'test_light': c.TX_TSL,
        'test_autonomy': c.TX_TSA,
        'test_even': c.TX_TAP,
        'test_odd': c.TX_TAI,
        'zone_in_test_on': c.TX_ZON,
        'zone_in_test_off': c.TX_ZOF,
        'reset_counters': c.TX_RPC,
        'error': c.TX_ERR
    }

    from_code_2_action_str = {v: k for k, v in from_action_str_2_code.items()}

    def __init__(self, light_id, action):
        self._light_id = light_id
        self.action = action

    def serialize_request(self):
        try:
            ACTION = self.from_action_str_2_code[self.action]
        except KeyError:
            raise KeyError('Bad action selected')

        id_to_send = self.light_id - 1

        # Broadcast is sent without - 1
        if self.action == 'zone_in_test_on' or self.action == 'zone_in_test_off' or self.light_id == 255 or self.action == 'test_even' or self.action == 'test_odd':
            id_to_send = 255

        payload = [id_to_send, c.TX_DAT, ACTION]
        return bytes(c.HEADER + payload + c.TAIL)

    @property
    def light_id(self):
        return self._light_id

    @light_id.setter
    def light_id(self, light_id):
        if light_id < 1 or light_id > 250 and light_id != 255:
            logger.warning('Bad light_id it should be between 1 and 250')
            raise ValueError('Bad light_id,  it should be between 1 and 250')
        else:
            self._light_id = light_id

    @property
    def action(self):
        return self._action

    @action.setter
    def action(self, action):
        if action not in self.allowed_actions:
            raise KeyError('Bad action selected')
        else:
            self._action = action

    def is_error(self):
        return self.action == 'error'

    def is_broadcast(self):
        return self.light_id == 255

    @staticmethod
    def build_packet_from_byte_sequence(stream):
        PacketAction.check_format(stream)

        light_id = stream[3]
        light_id = c.TX_DAT - light_id

        action = stream[4]
        try:
            return PacketAction(light_id, PacketAction.from_code_2_action_str[action])
        except KeyError:
            raise ex.BadActionException()

    @staticmethod
    def check_format(stream):
        if len(stream) < struct.calcsize(c.FMT_ACTION_RESPONSE):
            raise ex.BadLengthException()

        received_header = stream[0:3]
        received_tail = stream[5]

        if received_header != bytes(c.HEADER):
            logger.warning("Bad header. Current: {}, expected: {}".
                           format(received_header.hex(), bytes(c.HEADER).hex()))
            raise ex.BadHeaderException()

        if received_tail != c.TAIL[0]:
            logger.warning("Bad tail. Current: {}, expected: {}".
                           format(received_tail, bytes(c.TAIL).hex()))
            raise ex.BadTailException()

    @staticmethod
    def get_size(type):
        if type == 'request':
            return 7
        elif type == 'response':
            return 6


class PacketGetState:
    STATES = {
        'incandescent': 0x01,
        'test_light': 0x02,
        'test_test': 0x04,
        'emergency': 0x08,
        'test_autonomy': 0x10,
        'without_network': 0x20,
        'zdplus': 0x40,
        'present': 0x80
    }

    ALARMS = {
        'light_emergency': 0x01,
        'autonomy': 0x02,
        'power_supply':  0x04,
        'signaling': 0x08,
        'battery': 0x10
    }

    def __init__(self, light_id, states=None, alarms=None):
        self._light_id = light_id
        self._states = states
        self._alarms = alarms

    @property
    def light_id(self):
        return self._light_id

    @light_id.setter
    def light_id(self, light_id):
        if light_id < 1 or light_id > 250:
            logger.warning('Bad light_id it should be between 1 and 250')
            raise ValueError('Bad light_id,  it should be between 1 and 250')
        else:
            self._light_id = light_id

    @property
    def states(self):
        return self._states

    @states.setter
    def states(self, states):
        self._states = states

    @property
    def alarms(self):
        return self._alarms

    @alarms.setter
    def alarms(self, alarms):
        self._alarms = alarms

    def serialize_request(self):
        payload = [self.light_id - 1, c.TX_DAT, c.TX_EST]
        return bytes(c.HEADER + payload + c.TAIL)

    @staticmethod
    def build_packet_from_byte_sequence(stream):
        PacketGetState.check_format(stream)
        light_id = c.TX_DAT - stream[3]
        states = stream[4]
        alarms = stream[5]

        def decode_states(states):
            dict_states = {}
            for state, state_code in PacketGetState.STATES.items():
                dict_states[state] = (states & state_code) > 0

            return dict_states

        def decode_alarms(alarms):
            dict_alarms = {}
            for alarm, alarm_code in PacketGetState.ALARMS.items():
                dict_alarms[alarm] = (alarms & alarm_code) > 0

            return dict_alarms

        states = decode_states(states)
        alarms = decode_alarms(alarms)

        return PacketGetState(light_id=light_id, states=states, alarms=alarms)

    @staticmethod
    def check_format(stream):
        if len(stream) < struct.calcsize(c.FMT_GET_STATE):
            raise ex.BadLengthException()

        received_header = stream[0:3]
        received_tail = stream[6]

        if received_header != bytes(c.HEADER):
            logger.warning("Bad header. Current: {}, expected: {}".
                           format(received_header.hex(), bytes(c.HEADER).hex()))
            raise ex.BadHeaderException()

        if received_tail != c.TAIL[0]:
            logger.warning("Bad tail. Current: {}, expected: {}".
                           format(received_tail, bytes(c.TAIL).hex()))
            raise ex.BadTailException()

    @staticmethod
    def get_size(type):
        if type == 'request':
            return 7
        elif type == 'response':
            return 7


class PacketChangeLightId:
    def __init__(self, current_light_id, new_light_id):
        self.current_light_id = current_light_id
        self.new_light_id = new_light_id

    def serialize_request(self):
        payload = [self.current_light_id - 1, c.TX_CHL, self.new_light_id]
        return bytes(c.HEADER + payload + c.TAIL)

    @staticmethod
    def build_packet_from_byte_sequence(stream):
        PacketChangeLightId.check_format(stream)

        current_light_id = c.TX_CHL - stream[3]
        new_light_id = stream[4]

        return PacketChangeLightId(current_light_id, new_light_id)

    @staticmethod
    def check_format(stream):
        if len(stream) < struct.calcsize(c.FMT_CHANGE_ID):
            raise ex.BadLengthException()

        received_header = stream[0:3]
        received_tail = stream[5]

        if received_header != bytes(c.HEADER):
            logger.warning("Bad header. Current: {}, expected: {}".
                           format(received_header.hex(), bytes(c.HEADER).hex()))
            raise ex.BadHeaderException()

        if received_tail != c.TAIL[0]:
            logger.warning("Bad tail. Current: {}, expected: {}".
                           format(received_tail, bytes(c.TAIL).hex()))
            raise ex.BadTailException()

    @property
    def current_light_id(self):
        return self._current_light_id

    @current_light_id.setter
    def current_light_id(self, current_light_id):
        if current_light_id < 1 or current_light_id > 250:
            logger.warning('Bad light_id it should be between 1 and 250')
            raise ValueError('Bad light_id,  it should be between 1 and 250')
        else:
            self._current_light_id = current_light_id

    @property
    def new_light_id(self):
        return self._new_light_id

    @new_light_id.setter
    def new_light_id(self, new_light_id):
        if new_light_id < 1 or new_light_id > 250:
            logger.warning('Bad light_id it should be between 1 and 250')
            raise ValueError('Bad light_id,  it should be between 1 and 250')
        else:
            self._new_light_id = new_light_id

    @staticmethod
    def get_size(type):
        if type == 'request':
            return 7
        elif type == 'response':
            return 6


class PacketChangeInternalId:
    def __init__(self, new_internal_id):
        self.new_internal_id = new_internal_id

    @property
    def new_internal_id(self):
        return self._new_internal_id

    @new_internal_id.setter
    def new_internal_id(self, new_internal_id):
        if new_internal_id < 1 or new_internal_id > 250:
            logger.warning('Bad light_id it should be between 1 and 250')
            raise ValueError('Bad light_id,  it should be between 1 and 250')
        else:
            self._new_internal_id = new_internal_id

    def serialize_request(self):
        payload = [0xff, c.TX_CONF, self.new_internal_id]
        return bytes(c.HEADER + payload + c.TAIL)

    @staticmethod
    def build_packet_from_byte_sequence(stream):
        PacketChangeInternalId.check_format(stream)

        new_id = stream[4]

        return PacketChangeInternalId(new_id)

    @staticmethod
    def check_format(stream):
        if len(stream) < struct.calcsize(c.FMT_CHANGE_INTERNAL_ID):
            raise ex.BadLengthException()

        received_header = stream[0:3]
        received_tail = stream[5]

        if received_header != bytes(c.HEADER):
            logger.warning("Bad header. Current: {}, expected: {}".
                           format(received_header.hex(), bytes(c.HEADER).hex()))
            raise ex.BadHeaderException()

        if received_tail != c.TAIL[0]:
            logger.warning("Bad tail. Current: {}, expected: {}".
                           format(received_tail, bytes(c.TAIL).hex()))
            raise ex.BadTailException()

    @staticmethod
    def get_size(type):
        if type == 'request':
            return 7
        elif type == 'response':
            return 6


class PacketGetCounters:
    def __init__(self, light_id):
        self.light_id = light_id

    @property
    def light_id(self):
        return self._light_id

    @light_id.setter
    def light_id(self, light_id):
        if light_id < 1 or light_id > 250 and light_id != 255:
            logger.warning('Bad light_id it should be between 1 and 250')
            raise ValueError('Bad light_id,  it should be between 1 and 250')
        else:
            self._light_id = light_id

    @property
    def times_emergency(self):
        return self._times_emergency

    @times_emergency.setter
    def times_emergency(self, times_emergency):
        self._times_emergency = times_emergency

    @property
    def time_in_emergency(self):
        return self._time_emergency

    @time_in_emergency.setter
    def time_in_emergency(self, time_emergency):
        self._time_emergency = time_emergency

    def serialize_request(self):
        payload = [self.light_id - 1, c.TX_DAT, c.TX_HOR]
        return bytes(c.HEADER + payload + c.TAIL)

    @staticmethod
    def build_packet_from_byte_sequence(stream):
        PacketGetCounters.check_format(stream)

        light_id = stream[3]
        light_id = c.TX_DAT - light_id

        times_emergency, _, time_emergency = struct.unpack('>hch', stream[4:9])

        packet = PacketGetCounters(light_id)
        packet.times_emergency = times_emergency
        packet.time_in_emergency = time_emergency
        return packet

    @staticmethod
    def check_format(stream):
        if len(stream) < struct.calcsize(c.FMT_GET_COUNTERS):
            raise ex.BadLengthException()

        received_header = stream[0:3]
        received_tail = stream[9]

        if received_header != bytes(c.HEADER):
            logger.warning("Bad header. Current: {}, expected: {}".
                           format(received_header.hex(), bytes(c.HEADER).hex()))
            raise ex.BadHeaderException()

        if received_tail != c.TAIL[0]:
            logger.warning("Bad tail. Current: {}, expected: {}".
                           format(received_tail, bytes(c.TAIL).hex()))
            raise ex.BadTailException()

    @staticmethod
    def get_size(type):
        if type == 'request':
            return 7
        elif type == 'response':
            return struct.calcsize(c.FMT_GET_COUNTERS)


class PacketDiscover:
    def __init__(self):
        pass

    def serialize_request(self):
        payload = [c.TX_DAT, c.TX_DES]
        return bytes(c.HEADER + payload + c.TAIL)

    @staticmethod
    def build_packet_from_byte_sequence(stream):
        PacketDiscover.check_format(stream)
        return PacketDiscover()

    @staticmethod
    def check_format(stream):
        if len(stream) < struct.calcsize(c.FMT_PACKET_DISCOVER):
            print(struct.calcsize(c.FMT_PACKET_DISCOVER))
            raise ex.BadLengthException()

        received_header = stream[0:3]
        received_tail = stream[5]

        if received_header != bytes(c.HEADER):
            logger.warning("Bad header. Current: {}, expected: {}".
                           format(received_header.hex(), bytes(c.HEADER).hex()))
            raise ex.BadHeaderException()

        if received_tail != c.TAIL[0]:
            logger.warning("Bad tail. Current: {}, expected: {}".
                           format(received_tail, bytes(c.TAIL).hex()))
            raise ex.BadTailException()

        payload = stream[3:5]
        if payload != bytes([c.TX_DAT, c.TX_DES]):
            logger.warning("Payload {}. Expected {}".format(
                payload.hex(), [c.TX_DAT, c.TX_DES]))
            raise ex.BadPayloadException()

    @staticmethod
    def get_size(type):
        if type == 'request':
            return 6
        elif type == 'response':
            return 6


class PacketDiscoverAck:
    def __init__(self):
        self._state = -1

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, state):
        self._state = state

    @staticmethod
    def build_packet_from_byte_sequence(stream):
        PacketDiscoverAck.check_format(stream)
        state = stream[5]
        packet = PacketDiscoverAck()
        packet.state = state
        return packet

    @staticmethod
    def check_format(stream):
        if len(stream) < struct.calcsize(c.FMT_PACKET_DISCOVER):
            print(struct.calcsize(c.FMT_PACKET_DISCOVER))
            raise ex.BadLengthException()

        received_header = stream[0:3]
        received_tail = stream[6]

        if received_header != bytes(c.HEADER):
            logger.warning("Bad header. Current: {}, expected: {}".
                           format(received_header.hex(), bytes(c.HEADER).hex()))
            raise ex.BadHeaderException()

        if received_tail != c.TAIL[0]:
            logger.warning("Bad tail. Current: {}, expected: {}".
                           format(received_tail, bytes(c.TAIL).hex()))
            raise ex.BadTailException()

        payload = stream[3:5]
        if payload != bytes([c.TX_DAT, c.TX_ACK]):
            logger.warning("Payload {}. Expected {}".format(
                payload.hex(), [c.TX_DAT, c.TX_DES]))
            raise ex.BadPayloadException()

    @staticmethod
    def get_size(type):
        if type == 'response':
            return 7


class PacketRecognition:
    def __init__(self, initial_serial_number, last_serial_number):
        self.initial_serial_number = initial_serial_number
        self.last_serial_number = last_serial_number

    @property
    def initial_serial_number(self):
        return self._initial_serial_number

    @initial_serial_number.setter
    def initial_serial_number(self, initial_serial_number):
        self._initial_serial_number = initial_serial_number

    @property
    def last_serial_number(self):
        return self._last_serial_number

    @last_serial_number.setter
    def last_serial_number(self, last_serial_number):
        self._last_serial_number = last_serial_number

    @property
    def serial_number(self):
        return self._serial_number

    @serial_number.setter
    def serial_number(self, serial_number):
        self._serial_number = serial_number

    @property
    def light_id(self):
        return self._light_id

    @light_id.setter
    def light_id(self, light_id):
        self._light_id = light_id

    @property
    def repeater(self):
        return self._repeater

    @repeater.setter
    def repeater(self, repeater):
        self._repeater = repeater

    @property
    def hub_serial_number(self):
        return self._hub_serial_number

    @hub_serial_number.setter
    def hub_serial_number(self, hub_serial_number):
        self._hub_serial_number = hub_serial_number

    @property
    def power(self):
        return self._power

    @power.setter
    def power(self, power):
        self._power = power

    def is_last_packet(self):
        return self.serial_number == 0xffffffff

    def serialize_request(self):
        payload = [c.TX_DAT, c.TX_REC]
        range_of_id = struct.pack(
            '<II', self.initial_serial_number, self.last_serial_number)

        return bytes(c.HEADER) + bytes(payload) + range_of_id + bytes(c.TAIL)

    @staticmethod
    def build_packet_from_byte_sequence(stream):
        PacketRecognition.check_format(stream)

        serial_number, light_id, repeater, hub_serial_number, power = struct.unpack(
            '<IBBIB', stream[5:16])

        packet = PacketRecognition(0, 0)
        packet.serial_number = serial_number
        packet.light_id = light_id
        packet.repeater = repeater
        packet.hub_serial_number = hub_serial_number
        packet.power = power

        return packet

    @staticmethod
    def check_format(stream):
        if len(stream) < struct.calcsize(c.FMT_PACKET_RECOGNITION):
            print(struct.calcsize(c.FMT_PACKET_RECOGNITION))
            raise ex.BadLengthException()

        received_header = stream[0:3]
        received_tail = stream[16]

        if received_header != bytes(c.HEADER):
            logger.warning("Bad header. Current: {}, expected: {}".
                           format(received_header.hex(), bytes(c.HEADER).hex()))
            raise ex.BadHeaderException()

        if received_tail != c.TAIL[0]:
            logger.warning("Bad tail. Current: {}, expected: {}".
                           format(received_tail, bytes(c.TAIL).hex()))
            raise ex.BadTailException()

        payload = stream[3:5]
        if payload != bytes([c.TX_DAT, c.TX_REC]):
            logger.warning("Payload {}. Expected {}".format(
                payload.hex(), [c.TX_DAT, c.TX_DES]))
            raise ex.BadPayloadException()

    @staticmethod
    def get_size(type):
        if type == 'request':
            return 14
        elif type == 'response':
            return struct.calcsize(c.FMT_PACKET_RECOGNITION)


class PacketAssociation:
    def __init__(self, light_id, serial_number):
        self.serial_number = serial_number
        self.light_id = light_id

    @property
    def serial_number(self):
        return self._serial_number

    @serial_number.setter
    def serial_number(self, serial_number):
        self._serial_number = serial_number

    @property
    def light_id(self):
        return self._light_id

    @light_id.setter
    def light_id(self, light_id):
        self._light_id = light_id

    def serialize_request(self):
        payload = bytes([c.TX_DAT, c.TX_ASO])
        payload += struct.pack("<BI", self.light_id, self.serial_number)

        return bytes(c.HEADER) + payload + bytes(c.TAIL)

    @staticmethod
    def build_packet_from_byte_sequence(stream):
        PacketAssociation.check_format(stream)
        payload = stream[5:10]
        light_id, serial_number = struct.unpack("<BI", payload)
        result = PacketAssociation(light_id, serial_number)
        return result

    @staticmethod
    def check_format(stream):
        if len(stream) < struct.calcsize(c.FMT_PACKET_ASSOCIATION):
            print(struct.calcsize(c.FMT_PACKET_ASSOCIATION))
            raise ex.BadLengthException()

        received_header = stream[0:3]
        received_tail = stream[10]

        if received_header != bytes(c.HEADER):
            logger.warning("Bad header. Current: {}, expected: {}".
                           format(received_header.hex(), bytes(c.HEADER).hex()))
            raise ex.BadHeaderException()

        if received_tail != c.TAIL[0]:
            logger.warning("Bad tail. Current: {}, expected: {}".
                           format(received_tail, bytes(c.TAIL).hex()))
            raise ex.BadTailException()

        payload = stream[3:5]
        if payload != bytes([c.TX_DAT, c.TX_ASO]):
            logger.warning("Payload {}. Expected {}".format( payload.hex(), [c.TX_DAT, c.TX_ASO]))
            raise ex.BadPayloadException()

    @staticmethod
    def get_size(type):
        if type == 'request':
            return 11
        elif type == 'response':
            return 11

class PacketForcedAssociation:
    def __init__(self, light_id, serial_source, closest_serial):
        self.light_id = light_id
        self.serial_source = serial_source
        self.closest_serial = closest_serial

    @property
    def serial_source(self):
        return self._serial_source

    @serial_source.setter
    def serial_source(self, serial_source):
        self._serial_source = serial_source

    @property
    def closest_serial(self):
        return self._closest_serial

    @closest_serial.setter
    def closest_serial(self, closest_serial):
        self._closest_serial = closest_serial

    @property
    def light_id(self):
        return self._light_id

    @light_id.setter
    def light_id(self, light_id):
        self._light_id = light_id

    def serialize_request(self):
        payload = bytes([c.TX_DAT, c.TX_ASO_L])
        payload += struct.pack("<BII", self.light_id, self.closest_serial, self.serial_source)

        return bytes(c.HEADER) + payload + bytes(c.TAIL)

    @staticmethod
    def build_packet_from_byte_sequence(stream):
        PacketForcedAssociation.check_format(stream)
        payload = stream[5:10]
        light_id, serial_number = struct.unpack("<BI", payload)
        result = PacketForcedAssociation(light_id, serial_number, None)
        return result

    @staticmethod
    def check_format(stream):
        if len(stream) < struct.calcsize(c.FMT_PACKET_ASSOCIATION):
            print(struct.calcsize(c.FMT_PACKET_ASSOCIATION))
            raise ex.BadLengthException()

        received_header = stream[0:3]
        received_tail = stream[10]

        if received_header != bytes(c.HEADER):
            logger.warning("Bad header. Current: {}, expected: {}".
                           format(received_header.hex(), bytes(c.HEADER).hex()))
            raise ex.BadHeaderException()

        if received_tail != c.TAIL[0]:
            logger.warning("Bad tail. Current: {}, expected: {}".
                           format(received_tail, bytes(c.TAIL).hex()))
            raise ex.BadTailException()

        payload = stream[3:5]
        if payload != bytes([c.TX_DAT, c.TX_ASO]):
            logger.warning("Payload {}. Expected {}".format(
                payload.hex(), [c.TX_DAT, c.TX_ASO]))
            raise ex.BadPayloadException()

    @staticmethod
    def get_size(type):
        if type == 'request':
            return 15
        elif type == 'response':
            return 11


class PacketAssociationFinish:
    def __init__(self):
        pass

    def serialize_request(self):
        payload = [c.TX_DAT, c.TX_FASO]
        return bytes(c.HEADER + payload + c.TAIL)

    @staticmethod
    def build_packet_from_byte_sequence(stream):
        PacketAssociationFinish.check_format(stream)
        return PacketAssociationFinish()

    @staticmethod
    def check_format(stream):
        if len(stream) < struct.calcsize(c.FMT_PACKET_ASSOCIATION_FINISH):
            print(struct.calcsize(c.FMT_PACKET_ASSOCIATION_FINISH))
            raise ex.BadLengthException()

        received_header = stream[0:3]
        received_tail = stream[5]

        if received_header != bytes(c.HEADER):
            logger.warning("Bad header. Current: {}, expected: {}".
                           format(received_header.hex(), bytes(c.HEADER).hex()))
            raise ex.BadHeaderException()

        if received_tail != c.TAIL[0]:
            logger.warning("Bad tail. Current: {}, expected: {}".
                           format(received_tail, bytes(c.TAIL).hex()))
            raise ex.BadTailException()

        payload = stream[3:5]
        if payload != bytes([c.TX_DAT, c.TX_FASO]):
            logger.warning("Payload {}. Expected {}".format(
                payload.hex(), [c.TX_DAT, c.TX_DES]))
            raise ex.BadPayloadException()

    @staticmethod
    def get_size(type):
        if type == 'request':
            return 6
        elif type == 'response':
            return 6


class PacketDeleteLight:
    def __init__(self, light_id):
        self.light_id = light_id

    @property
    def light_id(self):
        return self._light_id

    @light_id.setter
    def light_id(self, light_id):
        self._light_id = light_id

    def serialize_request(self):
        payload = [self.light_id, c.TX_DAT, c.TX_DEL]
        return bytes(c.HEADER + payload + c.TAIL)

    @staticmethod
    def build_packet_from_byte_sequence(stream):
        PacketDeleteLight.check_format(stream)

        light = stream[3]
        return PacketDeleteLight(light)

    @staticmethod
    def check_format(stream):
        if len(stream) < struct.calcsize(c.FMT_PACKET_DELETE_LIGHT):
            print(struct.calcsize(c.FMT_PACKET_DELETE_LIGHT))
            raise ex.BadLengthException()

        received_header = stream[0:3]
        received_tail = stream[6]

        if received_header != bytes(c.HEADER):
            logger.warning("Bad header. Current: {}, expected: {}".
                           format(received_header.hex(), bytes(c.HEADER).hex()))
            raise ex.BadHeaderException()

        if received_tail != c.TAIL[0]:
            logger.warning("Bad tail. Current: {}, expected: {}".
                           format(received_tail, bytes(c.TAIL).hex()))
            raise ex.BadTailException()

        payload = stream[4:6]
        if payload != bytes([c.TX_DAT, c.TX_DEL]):
            logger.warning("Payload {}. Expected {}".format(
                payload.hex(), [c.TX_DAT, c.TX_DES]))
            raise ex.BadPayloadException()

    @staticmethod
    def get_size(type):
        if type == 'request':
            return 7
        elif type == 'response':
            return 7


class PacketGetHubId:
    def __init__(self):
        pass

    @property
    def hub_id(self):
        return self._hub_id

    @hub_id.setter
    def hub_id(self, hub_id):
        self._hub_id = hub_id

    @property
    def power(self):
        return self._power

    @power.setter
    def power(self, power):
        self._power = power

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, state):
        self._state = state

    def serialize_request(self):
        payload = [c.TX_DAT, c.TX_NCO]
        return bytes(c.HEADER + payload + c.TAIL)

    @staticmethod
    def build_packet_from_byte_sequence(stream):
        PacketGetHubId.check_format(stream)
        payload = stream[5:11]
        hub_id, power, state = struct.unpack('<IBB', payload)

        packet = PacketGetHubId()
        packet.hub_id = hub_id
        packet.power = power
        packet.state = state
        return packet

    @staticmethod
    def check_format(stream):
        if len(stream) < struct.calcsize(c.FMT_PACKET_GET_HUB_ID):
            raise ex.BadLengthException()

        received_header = stream[0:3]
        received_tail = stream[11]

        if received_header != bytes(c.HEADER):
            logger.warning("Bad header. Current: {}, expected: {}".
                           format(received_header.hex(), bytes(c.HEADER).hex()))
            raise ex.BadHeaderException()

        if received_tail != c.TAIL[0]:
            logger.warning("Bad tail. Current: {}, expected: {}".
                           format(received_tail, bytes(c.TAIL).hex()))
            raise ex.BadTailException()

        payload = stream[3:5]
        if payload != bytes([c.TX_DAT, c.TX_NCO]):
            logger.warning("Payload {}. Expected {}".format(
                payload.hex(), [c.TX_DAT, c.TX_DES]))
            raise ex.BadPayloadException()

    @staticmethod
    def get_size(type):
        if type == 'request':
            return 6
        elif type == 'response':
            return 12


class PacketSetLightRepeater:
    def __init__(self, light_id, repeater=False):
        self.light_id = light_id
        self.repeater = repeater

    @property
    def light_id(self):
        return self._light_id

    @light_id.setter
    def light_id(self, light_id):
        self._light_id = light_id

    @property
    def repeater(self):
        return self._repeater

    @repeater.setter
    def repeater(self, repeater):
        self._repeater = repeater

    def serialize_request(self):
        payload = [self.light_id - 1, c.TX_CONF]
        if self.repeater:
            payload.append(c.TX_REP)
        else:
            payload.append(c.TX_NRE)

        payload = bytes(payload)
        payload += struct.pack(">IIIII", 0, 0, 0, 0, 0)
        to_send = bytes(c.HEADER) + payload + bytes(c.TAIL)
        return to_send

    @staticmethod
    def build_packet_from_byte_sequence(stream):
        PacketSetLightRepeater.check_format(stream)
        light_id = stream[3]
        if stream[5] == c.TX_REP:
            repeater = True
        else:
            repeater = False

        return PacketSetLightRepeater(light_id, repeater)

    @staticmethod
    def check_format(stream):
        if len(stream) < struct.calcsize(c.FMT_PACKET_SET_REPEATER):
            print(struct.calcsize(c.FMT_PACKET_SET_REPEATER))
            raise ex.BadLengthException()

        received_header = stream[0:3]
        received_tail = stream[6]

        if received_header != bytes(c.HEADER):
            logger.warning("Bad header. Current: {}, expected: {}".
                           format(received_header.hex(), bytes(c.HEADER).hex()))
            raise ex.BadHeaderException()

        if received_tail != c.TAIL[0]:
            logger.warning("Bad tail. Current: {}, expected: {}".
                           format(received_tail, bytes(c.TAIL).hex()))
            raise ex.BadTailException()

        payload = stream[4]
        if payload != c.TX_CONF:
            logger.warning("Payload {}. Expected {}".format(
                payload.hex(), [c.TX_CONF]))
            raise ex.BadPayloadException()

    @staticmethod
    def get_size(type):
        if type == 'request':
            return 27
        elif type == 'response':
            return 7


class PacketDeleteCompleteHub:
    def __init__(self):
        pass

    def serialize_request(self):
        payload = [c.TX_DAT, c.TX_RST_HUB]
        payload = bytes(payload)
        to_send = bytes(c.HEADER) + payload + bytes(c.TAIL)
        return to_send

    @staticmethod
    def check_format(stream):
        if len(stream) < struct.calcsize(c.FMT_PACKET_DELETE_HUB):
            raise ex.BadLengthException()

        received_header = stream[0:3]
        received_tail = stream[5]

        if received_header != bytes(c.HEADER):
            logger.warning("Bad header. Current: {}, expected: {}".
                           format(received_header.hex(), bytes(c.HEADER).hex()))
            raise ex.BadHeaderException()

        if received_tail != c.TAIL[0]:
            logger.warning("Bad tail. Current: {}, expected: {}".
                           format(received_tail, bytes(c.TAIL).hex()))
            raise ex.BadTailException()

        payload = stream[3:5]
        if payload != bytes([c.TX_DAT, c.TX_RST_HUB]):
            logger.warning("Payload {}. Expected {}".format(
                payload.hex(), [c.TX_DAT, c.TX_DES]))
            raise ex.BadPayloadException()

    @staticmethod
    def build_packet_from_byte_sequence(stream):
        PacketDeleteCompleteHub.check_format(stream)
        return PacketDeleteCompleteHub()

    @staticmethod
    def get_size(type):
        if type == 'request':
            return 6

        else:
            return 6

class PacketChangeHubFrequency:
    def __init__(self, frequency):
        self.frequency = frequency

    @property
    def frequency(self):
        if self._frequency == 0:
            return 6
        else:
            return self._frequency

    @frequency.setter
    def frequency(self, frequency):
        if frequency < 1 or frequency > 6:
            raise TypeError("frequency must be between 1 and 6")

        # According to specification...
        if frequency == 6:
            self._frequency = 0
        else:
            self._frequency = frequency

    def serialize_request(self):
        payload = [c.TX_DAT, c.TX_CH_RF, self._frequency]
        return bytes(c.HEADER + payload + c.TAIL)

    @staticmethod
    def build_packet_from_byte_sequence(stream):
        PacketChangeHubFrequency.check_format(stream)
        frequency = stream[5]
        if frequency == 0:
            frequency = 6
        result = PacketChangeHubFrequency(frequency)
        return result

    @staticmethod
    def check_format(stream):
        if len(stream) < struct.calcsize(c.FMT_PACKET_CHANGE_FREQUENCY):
            raise ex.BadLengthException()

        received_header = stream[0:3]
        received_tail = stream[6]

        if received_header != bytes(c.HEADER):
            logger.warning("Bad header. Current: {}, expected: {}".
                           format(received_header.hex(), bytes(c.HEADER).hex()))
            raise ex.BadHeaderException()

        if received_tail != c.TAIL[0]:
            logger.warning("Bad tail. Current: {}, expected: {}".
                           format(received_tail, bytes(c.TAIL).hex()))
            raise ex.BadTailException()

        payload = stream[3:5]
        if payload != bytes([c.TX_DAT, c.TX_CH_RF]):
            logger.warning("Payload {}. Expected {}".format(
                payload.hex(), [c.TX_DAT, c.TX_CH_RF]))
            raise ex.BadPayloadException()

    @staticmethod
    def get_size(type):
        if type == 'request':
            return 7
        elif type == 'response':
            return 7
