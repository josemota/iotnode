class LibzpprotcolException(Exception):
    pass


class BadHeaderException(LibzpprotcolException):
    pass


class BadTailException(LibzpprotcolException):
    pass


class BadLengthException(LibzpprotcolException):
    pass


class BadActionException(LibzpprotcolException):
    pass


class BadPayloadException(LibzpprotcolException):
    pass
