import Ice

Ice.loadSlice('IoTNode.ice')
import IoTNode
import sys
import time
 
with Ice.initialize(sys.argv) as communicator:
    base = communicator.stringToProxy("zdOutput:tcp -h 127.0.0.1 -p 10003")
    zdoutput = IoTNode.ZDOutputServicePrx.checkedCast(base)
    if not zdoutput:
        raise RuntimeError("Invalid proxy")
 
    zdoutput.setLights(True)
    # time.sleep(1)
    # zdoutput.setLights(False)
    # time.sleep(1)
    # zdoutput.setLights(True)
    # time.sleep(1)
    # zdoutput.setLights(False)
