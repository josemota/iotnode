#!/usr/bin/python3
# -*- coding: utf-8; mode: python -*-

import sys
import Ice
import json
import logging
from xml.dom import minidom
import datetime

logging.basicConfig(stream=sys.stdout)
logger = logging.getLogger("SEM_ABS")
logger.setLevel(logging.INFO)

Ice.loadSlice('IoTNode.ice')
import IoTNode

class SemanticAbstractionServiceI(IoTNode.SemanticAbstractionService):
    def __init__(self, registry_prx, SCPCommMQTT_prx):
        self.registry = IoTNode.RegistryServicePrx.checkedCast(registry_prx)
        self.SCPCommMQTT = IoTNode.SCPCommunicationMQTTServicePrx.checkedCast(SCPCommMQTT_prx)

    def abstractMessage(self, localID, message, current = None):
        logger.info("message received for abstraction")

        if not self.registry.isRegistered(localID):
            logger.info("message from unregistered device")
            return
        
        normMsg = self.buildNormalizedMessage(localID, message)
        logger.info("normalized message built")
        self.sendNormalizedMessage(normMsg)      
        logger.info("message sent to communication service")


    def sendNormalizedMessage(self, normMsg):
        strMsg = normMsg.toprettyxml(encoding='UTF-8').decode()
        msgType = normMsg.getElementsByTagName("MessageType")[0].firstChild.data.lower()
        if msgType == "event":
            self.SCPCommMQTT.sendEvent(strMsg)
        elif msgType == "telemetry":
            self.SCPCommMQTT.sendTelemetry(strMsg)

    def buildNormalizedMessage(self, localID, message):
        #get config entry 
        devEntry = self.getDevEntry(localID) #FIXME:cambiar nombre deventry
        devType = self.getDevType(devEntry)
        
        #template creation
        normMsg = minidom.parseString(
            """
            <Object ObjectType="Reading">
                <GlobalID></GlobalID>
                <ObjectID></ObjectID>
                <Resources></Resources>
                <Metadata></Metadata>
                <MessageType></MessageType>
            </Object>
            """
        )

        # FIXME: format

        globalID = devEntry.getElementsByTagName("GlobalID")[0].firstChild.data
        node = normMsg.createTextNode(globalID)
        normMsg.getElementsByTagName("GlobalID")[0].appendChild(node)
        
        objectID = devType.getElementsByTagName("ObjectID")[0].firstChild.data
        node = normMsg.createTextNode(objectID)
        normMsg.getElementsByTagName("ObjectID")[0].appendChild(node)
        
        messageType = devEntry.getElementsByTagName("Reading")[0].getElementsByTagName("MessageType")[0].firstChild.data
        node = normMsg.createTextNode(messageType)
        normMsg.getElementsByTagName("MessageType")[0].appendChild(node)

        #normalizing every item in the reading
        items = devEntry.getElementsByTagName("Reading")[0].getElementsByTagName("Item")
        for item in items:
            itemID = item.attributes['ID'].value
            elem = normMsg.createElement("Item")
            elem.setAttribute("ID",itemID)
            value = self.getValue(devType, message, item)
            node = normMsg.createTextNode(str(value))
            elem.appendChild(node)
            normMsg.getElementsByTagName("Resources")[0].appendChild(elem)
        
        #normalizing metadata
        metaFieldNames = {"Timestamp", "Quality", "Expiration", "Latitude", "Longitude", "Altitude", "Place"}
        for field in metaFieldNames:
            metadata = devEntry.getElementsByTagName("Metadata")[0].getElementsByTagName(field)[0]
            #If the length is 0 it means this field is included in the reading
            if len(metadata.childNodes) == 0:
                readingMeta = devEntry.getElementsByTagName("ReadingMetadata")[0].getElementsByTagName(field)[0]
                metaValue = self.getMetaValue(message, readingMeta)
            
            #If the length is 1 it means the value is set textually in the field    
            elif len(metadata.childNodes) == 1:
                if field == "Quality" or field == "Expiration":
                    metaValue = int(metadata.firstChild.data)
                elif field == "Latitude" or field == "Longitude" or field == "Altitude":
                    metaValue = float(metadata.firstChild.data)
                elif field == "Timestamp":
                    metaValue = datetime.datetime.strptime(metadata.firstChild.data, '%Y-%m-%d %H:%M:%S.%f')
                else:
                    metaValue = str(metadata.firstChild.data)

            #If the length is more than 1, then it has a <calculated> tag
            else:
                if field == "Timestamp":
                    metaValue = datetime.datetime.now()
                elif field == "Quality":
                    #this should be the value returned from the method/service that calculates it
                    metaValue = int(255)
                elif field == "Expiration":
                    #this should be the value returned from the method/service that calculates it
                    metaValue = int(3600)
                elif field == "Latitude":
                    #FIXME: this should be the value returned from the method/service that calculates it
                    metaValue = float(38.9910394)
                elif field == "Longitude":
                    #FIXME: this should be the value returned from the method/service that calculates it
                    metaValue = float(-3.9200644)
                elif field == "Altitude":
                    #FIXME: this should be the value returned from the method/service that calculates it
                    metaValue = float(700)
                elif field == "Place":
                    #FIXME: this should be the value returned from the method/service that calculates it
                    metaValue = "ciudad_real.13071.paseo_de_la_universidad.4.escuela_superior_de_informatica.2.arco"


            elem = normMsg.createElement(field)
            node = normMsg.createTextNode(str(metaValue))
            elem.appendChild(node)
            normMsg.getElementsByTagName("Metadata")[0].appendChild(elem)

        # logger.info(normMsg.toprettyxml(encoding='UTF-8').decode())
        return normMsg

    def getMetaValue(self, message, readingMeta):
        #we already know the target data types for each metadata 
        originType = readingMeta.getElementsByTagName("DType")[0].firstChild.data.lower()

        #get the value in different ways depending of origin data type
        if originType == "string":
            location = readingMeta.getElementsByTagName("Location")[0].firstChild.data.split("-")
            strValue = message.decode()
            value = strValue[int(location[0]):(int(location[1])+1)]

            #once acquired the value, convert it to target data type    
            if readingMeta.tagName == "Timestamp":
                format = readingMeta.getElementsByTagName("Format")[0].firstChild.data
                
                #depending on the original format, different conversions may be done
                if format.lower() == "epoch":
                    value = datetime.datetime.fromtimestamp(int(value))
        
        return value


    def getValue(self, devType, message, item):
        originType = item.getElementsByTagName("DType")[0].firstChild.data.lower()
        
        #search for target data type
        for normItem in (devType.getElementsByTagName("Resources")[0].getElementsByTagName("Item")):
            if normItem.attributes['ID'].value == item.attributes['ID'].value:
                normType = normItem.getElementsByTagName("Type")[0].firstChild.data.lower()
                break

        #act depending on origin data type
        if originType == "string":
            location = item.getElementsByTagName("Location")[0].firstChild.data.split("-")
            strValue = message.decode()
            if location[0] != "" and location[1] != "":
                value = strValue[int(location[0]):(int(location[1])+1)]
            elif location[0] == "" and location[1] == "":
                value = strValue
            elif location[0] == "":
                value = strValue[:(int(location[1])+1)]
            elif location[1] == "":
                value = strValue[int(location[0]):]

            #convert to target data type
            if normType == "float":
                value = float(value)
            elif normType == "boolean":
                value = True if value == "True" else False

        return value

    def getDevEntry(self, localID):
        logger.info("getting device entry")
        strEntry = self.registry.getEntry(localID)
        devEntry = minidom.parseString(strEntry)
        return devEntry

    def getDevType(self, devEntry):
        logger.info("getting device type")
        IPSOType = devEntry.getElementsByTagName("Type")[0].firstChild.data
        strType = self.registry.getTypeDescription(IPSOType)
        devType = minidom.parseString(strType)
        return devType
        
class SEM_ABS(Ice.Application):
    def run(self, argv):
        logger.info("semantic abstraction service")
        ice = self.communicator()

        registryPrx = ice.propertyToProxy("Registry.Proxy")
        SCPCommMQTTPrx = ice.propertyToProxy("SCPCommMQTT.Proxy")

        servant = SemanticAbstractionServiceI(registryPrx, SCPCommMQTTPrx)
        adapter = ice.createObjectAdapter("Adapter")
        proxy = adapter.add(servant, ice.stringToIdentity("semAbs"))

        adapter.activate()
        self.shutdownOnInterrupt()
        ice.waitForShutdown()

        return 0

sem_abs = SEM_ABS()
sys.exit(sem_abs.main(sys.argv))
