all: down up

up: run-scp run-iotnode

down: down-scp down-iotnode

run-iotnode:
	docker-compose -f ./iotnode/docker-compose.yml up --build 

run-scp:
	docker-compose -f "./scp/docker-compose.yml" up --build &

down-iotnode:
	docker-compose -f "./iotnode/docker-compose.yml" down

down-scp:
	docker-compose -f "./scp/docker-compose.yml" down